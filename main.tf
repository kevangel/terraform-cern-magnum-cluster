# Defining the Magnum cluster resource to create Openstack kubernetes cluster

# Kubernetes cluster resource
# ffi: https://registry.terraform.io/providers/terraform-provider-openstack/openstack/1.50.0/docs/resources/containerinfra_cluster_v1
resource "openstack_containerinfra_cluster_v1" "cluster" {
  name                = var.cluster_name
  cluster_template_id = data.openstack_containerinfra_clustertemplate_v1.cluster_template.id
  keypair             = var.keypair

  # Master and minion node number corresponding to a machine
  master_count = var.master_count
  node_count   = var.node_count

  # Use cluster template values as default when the variables are not defined
  flavor        = try(var.flavor, data.openstack_containerinfra_clustertemplate_v1.cluster_template.flavor)
  master_flavor = try(var.master_flavor, data.openstack_containerinfra_clustertemplate_v1.cluster_template.master_flavor)

  # For creating certificates and keytab, 1 hour may not be enough
  create_timeout = var.create_timeout

  # The region in which to obtain the V1 Container Infra client.
  region = var.region

  # Merge custom labels with the cluster template default labels
  labels = merge(data.openstack_containerinfra_clustertemplate_v1.cluster_template.labels, var.labels)
}

# openstack_containerinfra_nodegroup_v1.nodegroup
# ffi: https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/containerinfra_nodegroup_v1
resource "openstack_containerinfra_nodegroup_v1" "nodegroup" {
  # for_each creates one instance for each member of that list
  for_each = { for ng in var.nodegroups_list : ng.name => ng }

  # The name of the OpenStack Magnum cluster.
  cluster_id = openstack_containerinfra_cluster_v1.cluster.id

  # Nodegroup attributes
  docker_volume_size = each.value.docker_volume_size
  flavor_id          = each.value.flavor_id
  image_id           = each.value.image_id
  labels             = each.value.labels
  max_node_count     = each.value.max_node_count
  merge_labels       = each.value.merge_labels
  min_node_count     = each.value.min_node_count
  name               = each.value.name
  node_count         = each.value.node_count
  region             = each.value.region
  role               = each.value.role
}